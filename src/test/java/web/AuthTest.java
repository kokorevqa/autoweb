package web;

import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import webPages.LoginPage;
import webPages.MyPostsPage;
import webPages.PostPage;

public class AuthTest extends AbstractTest {

    LoginPage loginPage;
    MyPostsPage myPostsPage;
    PostPage postPage;
    protected String username = "xyz";
    protected String password = "d16fb36f09";

    @BeforeEach
    void init() {
    loginPage = new LoginPage(getWebDriver());
    myPostsPage = new MyPostsPage(getWebDriver());
    postPage = new PostPage(getWebDriver());
}
    @Test
    @Story("Валидная авторизация")
    @DisplayName("Валидный логин 10 символов и валидный пароль ")
    public void test1() throws InterruptedException {
        loginPage.loginIn("qwerty1234","58b4e38f66");
        myPostsPage.checkChangeUrl();
    }

    @Test
    @Story("Валидная авторизация")
    @DisplayName("Валидный логин 3 символа и валидный пароль ")
    public void test2() throws InterruptedException {
        loginPage.loginIn("xyz","d16fb36f09");
        myPostsPage.checkChangeUrl();
    }
    @Test
    @Story("Валидная авторизация")
    @DisplayName("валидный логин 20 символов и валидный пароль ")
    public void test3() throws InterruptedException {
        loginPage.loginIn("qwerty12345678901234","aa71465486");
        myPostsPage.checkChangeUrl();
    }

    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Пустые логин и пароль ")
    public void test4() throws InterruptedException {
        loginPage.loginIn("","");
        loginPage.checkUrl();
    }
    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Невалидный логин 2 символа и валидный пароль")
    public void test6() throws InterruptedException {
        loginPage.loginIn("wq","2c204d8494");
        loginPage.checkUrl();
    }
    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Невалидный логин 21 символ и валидный пароль")
    public void test7() throws InterruptedException {
        loginPage.loginIn("qwertyqwerty123456789","2a764abdaa");
        loginPage.checkUrl();
    }

    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Невалидный логин на русском и валидный пароль")
    public void test8() throws InterruptedException {
        loginPage.loginIn("кверти","05223ed2c1");
        loginPage.checkUrl();
    }

    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Невалидный логин на русском с спецсимволом и валидный пароль")
    public void test9() throws InterruptedException {
        loginPage.loginIn("кверти123@","59eb9b38be");
        loginPage.checkUrl();
    }

    @Test
    @Story("НЕвалидная авторизация")
    @DisplayName("Невалидный логин на английском с спецсимволом и валидный пароль")
    public void test10() throws InterruptedException {
        loginPage.loginIn("qwerty@","b4f86c635e");
        loginPage.checkUrl();
    }

}
